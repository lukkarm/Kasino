#!usr/bin/python3.2
# -*- coding: utf-8 -*-

import unittest
from kasino.table import Table
import kasino.deck as deck
import logging
import logging.config

logging.config.fileConfig("kasino/logger.conf", disable_existing_loggers=False)
logger = logging.getLogger("kasino.test") 

class Test(unittest.TestCase):

	def setUp(self):
		self.table = Table()
		self.table.on_table = [deck.Card("S", 1), deck.Card("H", 1),
								deck.Card("S", 6), deck.Card("H", 10),
								deck.Card("D", 6), deck.Card("S", 4),
								deck.Card("S", 3), deck.Card("S", 5),
								deck.Card("S", 14)]
	
	def testSwapCards(self):
	
		card = deck.Card("C", 2)
		player = None
		cards = [deck.Card("S", 1), deck.Card("H", 1)]
		ret = self.table.swap(player, card, cards)
		self.assertEqual(1, ret)
		
		card = deck.Card("C", 7)
		player = None
		cards = [deck.Card("S", 4), deck.Card("S", 3), deck.Card("S", 1), deck.Card("D", 6)]
		ret = self.table.swap(player, card, cards)
		self.assertEqual(1, ret)
		
		card = deck.Card("C", 5)
		player = None
		cards = [deck.Card("S", 3), deck.Card("H", 1), deck.Card("S", 1)]
		ret = self.table.swap(player, card, cards)
		self.assertEqual(1, ret)
		
		card = deck.Card("S", 6)
		player = None
		cards = [deck.Card("S", 5), deck.Card("S", 14)]
		ret = self.table.swap(player, card, cards)
		self.assertEqual(1, ret)
		
if __name__ == '__main__':
	unittest.main()
