#!usr/bin/python3.2
# -*- coding: utf-8 -*-

import logging
import logging.config

import kasino.player as player

logger = logging.getLogger(__name__)



class AI(player.Player):
	def __init__(self, name):
		super().__init__(name)
		self.type = "AI"
		self.data = None

class AgressiveAI(AI):
	def __init__(self):
		super().__init__()
		self.AItype = self.__name__
		
class BlockAI(AI):
	""" AI for kasino game. Tries to block huts first then get best points """
	
	def __init__(self, name):
		super().__init__(name)
		self.AItype = self.__class__.__name__
		
	def calculate_move(self, table):
		logger.debug("AI {} calculating move".format(self.name))
		move = ""
		tablesum = 0
		for card in table.on_table:
			tablesum += card.value[1]
		
		if tablesum < 10:
			maxcard = max(card.value[0] for card in self.hand if card.number != 14) # Not including Aces
			maxcard = (card for card in self.hand if card.value[0] == maxcard)
			move += "{}".format(maxcard)
			return move	
		move = "0"
		return move # Same kind of string like human
		
# def mysum(currsum, tot, card, card_list, rec): #use with ai
			# """
				# Description here
			# """
			# if tot == card.value[0]: #Self.card.value[0] is the goal sum
				# for i in range(0, self.length):
					# subset = []
					# if rec[i]:
						# subset.append(self.card_list[i])
					
					# i += 1
					# for i in range(i, self.length):
						# if rec[i]:
							# subset.append(self.card_list[i])
					# self.possible.append(subset)
					# rec = [0 for i in range(0, len(self.card_list))]
					# return
					
			# i = currsum
			
			# for i in range(i, self.length):
			
				# if tot+self.card_list[i].value[1] > card.value[0]: # Too much, Skip
					# continue
				# if i>0 and self.card_list[i].value[1] == self.card_list[i-1].value[1] and not rec[i-1]: #
					# continue
				
				# rec[i] = 1 #Visited card i, mark it in rec
				# mysum(i+1, tot+self.card_list[i].value[1])
	
		# mysum(0, 0) #Put all possible combinations to self.possible
		
		# if len(self.possible) == 0:
			# logger.info("Invalid move") #Handle some other way
			# return 0
			
		# found = 0
		
		# for poss in self.possible:
			# for i in range(len(poss)):
				# for j in range(len(self.card_list)):
					# if poss[i] is not self.card_list[j] and poss[i].value[1] == self.card_list[j].value[1]:
						# new = poss[:]
						# new.pop(i)
						# new.append(self.card_list[j])
						# if new not in self.possible and list(set(new)) == new:
							# self.possible.append(new)
							