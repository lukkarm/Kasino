#!usr/bin/python3.2
# -*- coding: utf-8 -*-

import logging
import logging.config
import kasino.deck as deck

logger = logging.getLogger(__name__)

class Table:
	def __init__(self, emptyDeck = False):
		self.deck = deck.Deck(emptyDeck)
		self.deck.shuffle()
		self.on_table = []
		
	def newDeck(self):
		self.deck = deck.Deck()
		self.deck.shuffle()
		self.on_table = []
		
		
	def swap(self, player, card, cards): # Move to player Class (More logical there)
		"""
			Return: 1 if successful else 0
			Description Here
		"""

		
		logger.debug("{}.swap({}, {}, {})".format(self, player, card, cards))

		for item in cards:
			if item not in self.on_table:
				raise ValueError("Card {} not on table".format(item)) # Make better
		
		self.rec = [0 for i in range(0, len(self.on_table))] # For keeping track,
															 #Tells which items in tablecards are in sum
		self.length = len(self.on_table)
		self.possible = []
		
		# self.on_table is sorted

		self.card_list = sorted(self.on_table, key = lambda card: card.value[1], reverse = True)

		#self.on_table.sort(key = lambda card: card.value[1], reverse = True)
		# Use this for AI?
		def mysum(currsum, tot):
			"""
				Description here
			"""
			if tot == card.value[0]: #Self.card.value[0] is the goal sum
				for i in range(0, self.length):
					subset = []
					if self.rec[i]:
						subset.append(self.card_list[i])
					
					i += 1
					for i in range(i, self.length):
						if self.rec[i]:
							subset.append(self.card_list[i])
					self.possible.append(subset)
					self.rec = [0 for i in range(0, len(self.card_list))]
					return
					
			i = currsum
			
			for i in range(i, self.length):
			
				if tot + self.card_list[i].value[1] > card.value[0]: # Too much, Skip
					continue
				if i > 0 and self.card_list[i].value[1] == self.card_list[i-1].value[1] and not self.rec[i-1]: #
					continue
				
				self.rec[i] = 1 #Visited card i, mark it in rec
				mysum(i+1, tot+self.card_list[i].value[1])
	
		mysum(0, 0) #Put all possible combinations to self.possible
		
		if len(self.possible) == 0:
			logger.info("Invalid move") #Handle some other way
			return 0
			
		for poss in self.possible: # Constructing "duplicates"
			for i in range(len(poss)):
				for j in range(len(self.card_list)):
					if poss[i] != self.card_list[j] and poss[i].value[1] == self.card_list[j].value[1]:
						new = poss[:]
						new.pop(i)
						new.append(self.card_list[j])
						new.sort(key = lambda card: (card.value[1], card.value[0]), reverse = True)
						if new not in self.possible and len(list(set(new))) == len(new):
							self.possible.append(new)
							
		logger.debug("Possible combinations for sum of {}:\n{}".format(card.value[0], self.possible))
		
		#Fix this
		#Must empty cards list using possible sum combinations
		cards.sort(key = lambda card: card.value[1], reverse = True)
		temp = cards[:]
		logger.debug("cards: {}".format(temp))
		for i in range(len(self.possible)):
			poss = sorted(self.possible[i], key = lambda card: (card.value[1], card.value[0]), reverse = True)
			logger.debug("POSS: {}".format(poss))
			
			if poss == [x for x in poss if x in temp]:
			#if poss in temp:
				for x in poss:
					temp.remove(x)
				#temp = temp - poss
				logger.debug("temp: {}".format(temp))
				
			if temp == []:
				return 1
		
		#for poss in self.possible:
		#	if poss.sort(key = lambda card: card.value[1], reverse = True) in cards:
				#Mark points for player, perform swap
		#		found = 1
		#if found:
		#	return 1
		return 0
		
	def print_table(self):
		j = 0
		print("On table")
		str1 = ""
		str2 = ""
		#arr = []
		for i in range(len(self.on_table)):
			if j > 0 and j % 10 == 0:
				print(str1)
				print(str2)
				str1 , str2 = "", ""
				
			str1 += "{}	".format(self.on_table[i])
			str2 += "[{}]	".format(i)
			j += 1
		print(str1)
		print(str2)
		

		