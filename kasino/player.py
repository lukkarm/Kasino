#!usr/bin/python3.2
# -*- coding: utf-8 -*-

import logging
import logging.config


logger = logging.getLogger(__name__)

class Player:
	def __init__(self, name):
		self.name = name
		self.hand = []
		self.points = 0
		self.stack = []
		self.number = None
		
	def print_hand(self):
		print("{}'s hand".format(self.name))
		for card in self.hand:
			print("{}	".format(card), end = " ")
		print()
		
		string = ""
		for i in range(len(self.hand)):
			string += "[{}]	".format(i)
		print(string)

class HumanPlayer(Player):
	def __init__(self, name):
		super().__init__(name)
		self.type = "Human"

