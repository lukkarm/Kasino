#!usr/bin/python3.2
# -*- coding: utf-8 -*-

import random
import logging
import logging.config

logger = logging.getLogger(__name__)


class Card:

	cardvalues = {14:(14, 1), "D10":(16, 10), "S2":(15, 2)} #Cardvalues in tuple (inhand, ontable)
	
	def __init__(self, suit, number):
	
		if (suit.upper() not in "SHDC") and (number not in range(2,15)):
			logger.debug("Card({}, {}) Raised error".format(suit, number)) #Better way to handle?
			raise ValueError #Make own error?
	
		self.number = int(number)
		self.suit = suit.upper()
	
		if self.number == 14:
			self.value = self.cardvalues[14] #self.value = (inhand, table)
		else:
			self.value = self.cardvalues.get("{}{}".format(suit, number), (self.number, self.number))
			
	def __str__(self):
		return "{}{}".format(self.suit, self.number)
		
	def __repr__(self):
		return "Card(\"{}\", {})".format(self.suit, self.number)
		
	def __eq__(self, other):
		if isinstance(other, Card):
			return self.__dict__ == other.__dict__
		
			#if self.value == other.value and self.suit == other.suit:
			#	return True
			#return False
		return False
	def __ne__(self, other):
		return not self.__eq__(other)
		
	def __hash__(self):
		return hash(self.__repr__())
	
class Deck:
	def __init__(self, empty = False):
		self.deck = []
		if not empty:
			for i in "SCDH":
				for l in range(2,15):
					self.deck.append(Card(i, l))
	
	def add(self, card):
		if isinstance(card, Card):
			self.deck.append(card)
		else:
			raise ValueError # Own error here?
					
	def shuffle(self):
		random.shuffle(self.deck)
		
	def deal(self):
		return self.deck.pop()
		
	def __iter__(self):
		for card in self.deck:
			yield card
			
	def __len__(self):
		return len(self.deck)