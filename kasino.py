#!usr/bin/python3.2
# -*- coding: utf-8 -*-

import logging
import logging.config
import time
import sys

from PyQt5.QtCore import Qt, QSize
from PyQt5.QtWidgets import (QApplication, QGridLayout, QLayout, QLineEdit,
        QSizePolicy, QToolButton, QWidget, QPushButton, QAbstractButton)
from PyQt5.QtGui import QPainter, QPixmap

import kasino.table as tb
import kasino.player as pl
import kasino.deck as dck
import kasino.ai as ai



logging.config.fileConfig("kasino/logger.conf", disable_existing_loggers=False)
logger = logging.getLogger("kasino") 
#logger.setLevel(logging.CRITICAL)

class Kasino:

	def __init__(self):
		self.table = tb.Table()
		self.players = []
		self.VERSION = "V0.1"
		self.player_turn = 0
		self.round = 0
				
	def newGame(self):
		
		self.player_len = 0
		ai_len = None
		while self.player_len < 2:
			try:
				self.player_len = int(input("How many players: "))
			except ValueError:
				print("Invalid value")
				
		while ai_len is None:	
			try:
				ai_len = int(input("number of AIs: "))
			except ValueError:
				print("Invalid value")
				
		for i in range(self.player_len): #Add players
			player = pl.HumanPlayer(input("Player {} name: ".format(i+1)))
			player.number = i
			self.players.append(player)
		
		for i in range(ai_len):
			player = ai.BlockAI("Jorma")
			player.number = i+self.player_len
			self.players.append(player)
			
		self.player_len += ai_len
		
		self.start_time = time.time()
		self.gameLoop()
		
	def gameLoop(self):
		
				
		for i in range(4):
			self.table.on_table.append(self.table.deck.deal())
		
		logger.debug("On table {}".format(self.table.on_table))
		running = 1
		
		while 16 > max([player.points for player in self.players]): # Running till player gets 16+ points
			self.round += 1
			players_out = 0
			while running: # MAINLOOP. Round running
				
				if len(self.table.deck):
					for player in self.players:
						while len(player.hand) < 4:
							player.hand.append(self.table.deck.deal())
						logger.debug("{}'s hand: {}".format(player.name, player.hand))
				self.player_turn = 0
				
				while self.player_turn < self.player_len:
					player = self.players[self.player_turn]
					if len(player.hand) == 0:
						players_out += 1
						self.player_turn += 1
						continue
				
					self.table.print_table()
					player.print_hand()
					
					ret = 0
					while ret == 0:
						if player.type == "AI":
							move = player.calculate_move(self.table)
							print(move)
						else:
							move = input("<card #> <to swap #s separated by , or empty> ex. 2 3,5: ")
							if len(move) == 0:
								self.table.print_table()
								player.print_hand()
								continue
						
						move = move.split(" ")						
						
						if move[0].lower() == "save":
							self.save()
							move = input("<card #> <to swap #s separated by , or empty> ex. 2 3,5: ")
							if len(move) == 0:
								continue
							move = move.split(" ")
						elif move[0].lower() == "load":
							#self.player_turn = 0
							self.load()
							#self.table.print_table
							if self.player_turn == 0:
								self.player_turn = self.player_len
							else:
								self.player_turn -= 1
							break
							move = input("<card #> <to swap #s separated by , or empty> ex. 2 3,5: ")
							if len(move) == 0:
								continue
							
							move = move.split(" ")
						elif move[0].lower() == "quit":
							sys.exit()
						
						if len(move) == 1:
							try:
								move[0] = int(move[0])
								if 0 <= move[0] < len(player.hand):
									self.table.on_table.append(player.hand.pop(int(move[0])))
									break
								else:
									print("Invalid index")
							except ValueError:
								print("invalid input")
						else:
							card = player.hand[(int(move[0]))]
							cards = []
							indexes = [int(x) for x in move[1].split(",") if (0 <= int(x) < len(self.table.on_table))]
							logger.debug("Indexes: {}".format(indexes))
							
							for i in range(len(indexes)):
								
								cards.append(self.table.on_table[int(indexes[i])])
									
							ret = self.table.swap(player, card, cards)
							
							if ret: # Should this be in table class ("swap" method)
								if len(indexes) == len(self.table.on_table):
									player.points += 1 #Mökki
								logger.debug("Swapping {} with:".format(player.hand[(int(move[0]))]))
								self.table.on_table.append(player.hand.pop(int(move[0]))) # Card from player
								for i in sorted(indexes, reverse = True):
									logger.debug(self.table.on_table[i])
									player.stack.append(self.table.on_table.pop(i))
									
									
								logger.debug("{}'s stack: {}".format(player.name, player.stack))
							else:
								print("Invalid move")
								
					self.player_turn += 1
					
				if players_out >= self.player_len:
					#running = 0
					break #Round over
					
				
			self.calculatePoints()

			for player in self.players:
				print("{}'s points: {}".format(player.name, player.points))
		
			self.table.newDeck()
			for i in range(4):
				self.table.on_table.append(self.table.deck.deal())
			running = 1 #New round
			
		
		self.players = sorted(self.players, key = lambda player: player.points, reverse = True)
		print("Winner is {} with {} points".format(self.players[0].name, self.players[0].points))
		for player in self.players:
			print("{}'s points: {}".format(player.name, player.points))

	def calculatePoints(self):		
		for player in self.players:
			if len(player.stack) == 0:
				continue
			for card in player.stack:
				if card == dck.Card("D", 10):
					player.points += 2
					print("{} got 2 points for {}".format(player.name, card))
				if card == dck.Card("S", 2):
					player.points += 1
					print("{} got 1 points for {}".format(player.name, card))
				if card.number == 14:
					player.points += 1
					print("{} got 1 points for {}".format(player.name, card))
			
			if len(player.stack) == max([len(player.stack) for player in self.players]):
				player.points += 1
				print("{} got 1 points for most cards".format(player.name))
				
			if len([card for card in player.stack if card.suit == "S"]) == \
				max([len([card for card in player.stack if card.suit == "S"]) for player in self.players]): #FIX THIS
				player.points += 2
				print("{} got 2 points for most Spades".format(player.name))
			

	def save(self, filename = "kasino.sav"): # add deck saving and stack saving
		with open(filename, 'w') as file: # TODO: Check if exists
			file.write("{} KASINO\n".format(self.VERSION))
			file.write("Tiedot\n")
			file.write("\tpelaajia = {}\n".format(len([player for player in self.players if player.type == "Human"])))
			file.write("\ttekoälyjä = {}\n".format(len([player for player in self.players if player.type == "AI"])))
			file.write("\tvuorossa = {}\n".format(self.player_turn))
			file.write("\tkierros = {}\n".format(self.round))
			file.write("\taika = {}\n".format(time.time() - self.start_time)) # When load save this to start_time
			
			file.write("Pöytä\n")
			file.write("\tkortit = {}\n".format(" ".join(str(card) for card in self.table.on_table))) #?
			
			file.write("Pakka\n")
			file.write("\tkortit = {}\n".format(" ".join(str(card) for card in self.table.deck))) #?
			
			
			for player in self.players: # No need for numero saved in order
				if player.type == "Human":
					file.write("Pelaaja\n")
				else:
					file.write("Tekoäly\n")
				file.write("\tnimi = {}\n".format(player.name))
				file.write("\tkortit = {}\n".format(" ".join(str(card) for card in player.hand)))
				file.write("\tpino = {}\n".format(" ".join(str(card) for card in player.stack)))
				file.write("\tpisteet = {}\n".format(player.points))
				file.write("\tnumero = {}\n".format(player.number))
				if player.type == "AI":
					file.write("\ttype = {}\n".format(player.AItype))
					file.write("\tdata = {}\n".format(player.data))
		
		print("Saved {}".format(filename))
		
		
	def load(self, filename = "kasino.sav"):
		data = {"Tiedot": {
							"pelaajia": 0,
							"tekoälyjä": 0,
							"vuorossa": 0,
							"kierros": 0,
							"aika": 0.0},
				"Pöytä": {
							"kortit": ""},
				"Pakka": {
							"kortit": ""},
				"players": []
				}
				
		with open(filename, 'r') as file:
			logger.debug("Loading save {}".format(filename))
			header = file.readline()
			logger.debug(header)
			if header.split(" ")[1].strip() == "KASINO":
				header = ""
				for line in file:
					player_template = {"Pelaaja": {
													"nimi": "",
													"kortit": "",
													"pino": "",
													"numero": None,
													"pisteet": 0}
													}
					AI_template = {"Tekoäly":	{
													"nimi": "",
													"kortit": "",
													"pino": "",
													"numero": None,
													"pisteet": 0,
													"type": "",
													"data": "",
													}
										}
										
					template = {"Pelaaja": player_template, "Tekoäly": AI_template}		
					number = {"Pelaaja": 5, "Tekoäly": 7}
					
					if header == "" or len(line.split("=")) == 1:
						header = line.strip()
						logger.debug(header)
					elif header == "Pelaaja" or header == "Tekoäly":
						for i in range(number[header]): 
							try:
								line = line.split("=")
								logger.debug(line)
								template[header][header][line[0].strip()] = line[1].strip()
							except Exception:
								raise IOError #MAKE OWN
							line = file.readline()
						data["players"].append(template[header])
					else:
						try:
							line = line.split("=")
							logger.debug(line)
							data[header][line[0].strip()] = line[1].strip()
						except Exception:
							raise IOError #MAKE OWN
			
			# CHECK DATA AND ERROR IF FAULTY?
			
			self.player_len = int(data["Tiedot"]["pelaajia"]) + int(data["Tiedot"]["tekoälyjä"])
			self.player_turn = int(data["Tiedot"]["vuorossa"])
			self.round = int(data["Tiedot"]["kierros"])
			self.start_time = float(data["Tiedot"]["aika"])
			
			self.table = tb.Table(emptyDeck = True)
			for item in data["Pöytä"]["kortit"].split(" "): #TABLE
				self.table.on_table.append(dck.Card(item[0], item[1:]))
			
			for item in data["Pakka"]["kortit"].split(" "): #DECK
				self.table.deck.add(dck.Card(item[0], item[1:]))
				
			
			self.players = []
			for playerdata in data["players"]: #PLAYERS
				if "Pelaaja" in playerdata.keys():
					player = pl.HumanPlayer(playerdata["Pelaaja"]["nimi"])
					player.points = playerdata["Pelaaja"]["pisteet"]
					player.hand = [dck.Card(card[0], card[1:]) for card in playerdata["Pelaaja"]["kortit"].split(" ")]
					player.stack = [dck.Card(card[0], card[1:]) for card in playerdata["Pelaaja"]["pino"].split(" ")]
					player.number = int(playerdata["Pelaaja"]["numero"])
					self.players.append(player)

				elif "Tekoäly" in playerdata.keys(): # FIX this
					player = pl.HumanPlayer(playerdata["Tekoäly"]["nimi"])
					player.points = playerdata["Tekoäly"]["pisteet"]
					player.hand = [dck.Card(card[0], card[1:]) for card in playerdata["Tekoäly"]["kortit"].split(" ")]
					player.stack = [dck.Card(card[0], card[1:]) for card in playerdata["Tekoäly"]["pino"].split(" ")]
					player.data = playerdata["Tekoäly"]["data"]
					player.AItype = playerdata["Tekoäly"]["type"]
					player.number = int(playerdata["Tekoäly"]["numero"])
					self.players.append(player)

			self.players.sort(key = lambda player: player.number)		
			print("Loaded save: {}".format(filename))
			
			



class KasinoGUI(QWidget):
	def __init__(self):
		super().__init__()
		self.kasino = Kasino()
		self.initUI()
		
	def initUI(self):
		self.grid = QGridLayout()
		self.setLayout(self.grid)
		
		self.table_buttons = [[],[],[],[]]
		for j in range(4):
			for i in range(13):
				button = CardButton("Empty", "kasino/icons/card.png", self)
				button.setCheckable(True)
				self.grid.addWidget(button, j, i)
				self.table_buttons[j].append(button)
		
		
		self.player_buttons = []
		for i in range(4):
			button = CardButton("Empty_player", "kasino/icons/card.png", self)
			button.setCheckable(True)
			self.grid.addWidget(button, 6, i)
			self.player_buttons.append(button)
			
		self.submit_button = QPushButton("Submit")
		self.grid.addWidget(self.submit_button, 6, 15)
		
		self.setWindowTitle("Kasino")
		self.show()
		
class CardButton(QAbstractButton):
	def __init__(self, text, pixmap, parent = None):
		super().__init__(parent)
		self.text = text
		self.setText(self.text)
		self.pixmap = QPixmap(pixmap)
		
	def sizeHint(self):
		return QSize(40, 60)
		
	def paintEvent(self, event):
		pixmap = self.pixmap
		if self.isChecked():
			pixmap = QPixmap("kasino/icons/card_pressed.png")
		painter = QPainter(self)
		painter.drawPixmap(event.rect(), pixmap)
		
if __name__ == "__main__":
	app = QApplication([])
	kasino = KasinoGUI()
	sys.exit(app.exec_())